import React, { Component } from 'react';
import * as Api from '../../utils/ApiProxy';

class WinesShipping extends Component {
  constructor(props){
    super(props);
    this.state = {
      wines: []
    }
  }

  componentDidMount() {
    const nsId = this.props.nsId;
    Api.get('customers/' + nsId + '/pendingShipmentsJJBWines')
    .then(data => {
      let wines = data.map((item) => {
        return (
          <tr key={item.ItemId + item.ShipDate}>
              <td>{item.Year}</td>
              <td>{item.WineName}</td>
              <td>{item.Varietal}</td>
              <td>{item.Size}</td>
              <td>{item.CurrentQty}</td>
              <td>{item.ShipMethod}</td>
              <td>{item.ShipDate}</td>
          </tr>
        )
      });
      
      this.setState({wines: wines});
    })
  }

  render() {
    return (
          <table className="table table-striped table-hover table-condensed">
              <thead>
                  <tr>
                      <th>Vintage</th>
                      <th>Wine</th>
                      <th>Varietal</th>
                      <th>Size</th>
                      <th>Qty</th>
                      <th>Method</th>
                      <th>Date</th>
                  </tr>
              </thead>

              <tbody>
              {this.state.wines}
              </tbody>
          </table>

    );
  }
}

export default WinesShipping;
