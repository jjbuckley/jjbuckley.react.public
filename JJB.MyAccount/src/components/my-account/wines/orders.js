import React, { Component } from 'react';
import { Alert } from 'react-bootstrap/lib/';
import * as Api from '../../utils/ApiProxy';

class Orders extends Component{
    constructor(props){
        super(props);
        this.state = {
          orders: null,
          errors: null,
        }
        this.setOrders = this.setOrders.bind(this);
      }
    
    componentDidMount() {
        this.setOrders();
    }

    setOrders() {
        const nsId = this.props.user.CustomerId;
        console.log("updating at Orders in account " + nsId );
        Api.get('customers/' + nsId + '/orders')
        .then(data => {
          this.setState({orders: data, errors: null});
        })
        .catch(err => {
          console.log(err)
          this.setState({errors: err});
        })
      }

    render(){
        const orders = this.state.orders;
        const errors = this.state.errors;
        const dateOptions = {month:'short', day:'2-digit', year:'numeric'};
        return (
        <div>
        {errors ?
            <Alert bsStyle="danger">{ errors.statusText } ({ errors.status })</Alert>
            :
            orders ? 
                <div className="table-responsive">
                    <table className="table table-hover">
                        <thead>
                            <tr>
                                <th>Order Date</th>
                                <th>Order Number</th>
                                <th>Items</th>
                                <th>Total</th>
                                <th>Payment Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            {orders.map((order) => {
                                return(
                                <tr key={ order.Id }>
                                    <td>{ new Date(order.OrderDate).toLocaleString('en-US', dateOptions) }</td>
                                    <td><a href={"#" + order.Id} onClick={this.props.setOrder.bind(this, order.Id)}>{ order.OrderId }</a></td>
                                    <td>{ order.ItemsQuantity }</td>
                                    <td>${ order.Total }</td>
                                    <td>{ order.PaymentState }</td>
                                </tr>
                                )
                            })}
                        </tbody>
                    </table>
                    </div>
                :
                <div className="text-center">
                    <div className="loader-circle-head big gray" id="spinner">
                    </div>
                </div>
            
        }
        </div>
        ) 
    }
}

export default Orders;