export function handleResponse (response) {
    if (!response.ok || response.status >= 300) {
       return Promise.reject(response)
    }
    return response.json();
};

export function checkToken() {
    let token = false;
    let expireDate = parseInt(localStorage.getItem('api_expires_in'), 10);

    if (expireDate && expireDate > Date.now()) {
        token = localStorage.getItem('api_access_token');
    }
    if(!token) {
        token = getToken();
    }
    
    return token;
}

export function getToken() {
    let apiUrl =  process.env.REACT_APP_JJBBASE_URL + 'token';
    let authData = {
        grant_type: 'password',
        username: process.env.REACT_APP_JJBAPI_USER_NAME,
        password: process.env.REACT_APP_JJBAPI_USER_PASSWORDREACT_APP_JJBAPI_USER_PASSWORD
    }
    var formBody = [];
    for (let property in authData) {
        let encodedKey = encodeURIComponent(property);
        let encodedValue = encodeURIComponent(authData[property]);
        formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");

    fetch(apiUrl, {
        method: 'POST',
        mode: 'cors',
        body: formBody,
        headers: new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        })
    })
    .then(handleResponse)
    .then(data => {
        console.log('setting token after request');
        console.log(data)
        localStorage.setItem('api_access_token', data.access_token);
        localStorage.setItem('api_expires_in', data.expires_in * 1000 + Date.now());
        localStorage.setItem('api_token_type', data.token_type);
        return data;
    })
    .catch(data => {
        throw data;
    })

}

export function get(resource) {
    let authHash = checkToken();
    let apiUrl = process.env.REACT_APP_JJBAPI_URL + resource;

    return fetch(apiUrl, {
        headers: new Headers({
            "Authorization": "Bearer " + authHash,
            'Content-Type': 'application/json'
        })
    })
    .then(handleResponse)
    .catch(data => {
        throw data;
    })
}

export function post(resource, body) {
    let authHash = checkToken();
    let apiUrl = process.env.REACT_APP_JJBAPI_URL + resource;

    return fetch(apiUrl, {
        method: 'POST',
        headers: new Headers({
            "Authorization": "Bearer " + authHash,
            'Content-Type': 'application/json'
        }),
        body: JSON.stringify(body)
    })
    .then(handleResponse)
    .catch(data => {
        throw data;
    })
}