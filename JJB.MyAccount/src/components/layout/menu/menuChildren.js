import React, { Component } from 'react';
import {MenuItem, NavDropdown} from 'react-bootstrap/lib/';

class MenuChildren extends Component {

    render() {
        const item = this.props.item;
        const hasChilds = item.children && item.children.length > 0;

        return (
            <NavDropdown title={item.title} id={item.title} className={this.props.hasSubChildren ? 'hasCols' : null} href={item.friendlyUrl}>
                {hasChilds ? 
                    item.children.map((subitem) => {
                        return(subitem.children && subitem.children.length >= 1 ?
                            <div className="submenu" key={subitem.title}>
                                <MenuItem header>{subitem.title}</MenuItem>
                                {subitem.children.map((subsubitem) => {
                                    return(<MenuItem key={subsubitem.url} href={subsubitem.friendlyUrl}>{subsubitem.title}</MenuItem>)
                                })}
                            </div>
                        :
                            <MenuItem key={subitem.title} href={subitem.friendlyUrl}>{subitem.title}</MenuItem>
                        )
                    }) 
                : null}
            </NavDropdown>
        );
    }
}
             
export default MenuChildren;