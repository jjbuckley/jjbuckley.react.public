import React, { Component } from 'react';
import WinesJJB from "./winesJJB.js";
import WinesPending from "./winesPending.js";
import { Row, Col, Nav, NavItem, Tab } from 'react-bootstrap/lib/';

class MyWines extends Component {
    constructor(props) {
        super(props);
        this.state = {
            statsAtt: null,
            statsPending: null,
            statsShipping: null,
            socketMessaging: "no message",
            socketMessaginToUpdate : false
        }
        this.setTabLegend = this.setTabLegend.bind(this);
        this.renderMessageFromStock = this.renderMessageFromStock.bind(this);
    }

    setTabLegend(tab, legend) {
        if (tab === 1) {
            this.setState({statsAtt: legend})      
        }
        if (tab === 2) {
            this.setState({statsPending: legend})                   
        }
        if (tab === 3) {
            this.setState({statsShipping: legend})                   
        }
    }

    componentDidMount() {

        var url = process.env.REACT_APP_JJBSOCKET_REFRESH + this.props.user.CustomerId;
        let ws = new WebSocket(url);

        ws.onmessage = this.renderMessageFromStock;
  
        this.props.setTitle('My Wines')
    }

    getTIMESTAMP(dateTime) {

        var date = new Date();
        var year = date.getFullYear();
        var month = ("0"+(date.getMonth()+1)).substr(-2);
        var day = ("0"+date.getDate()).substr(-2);
        var hour = ("0"+date.getHours()).substr(-2);
        var minutes = ("0"+date.getMinutes()).substr(-2);
        var seconds = ("0"+date.getSeconds()).substr(-2);
        
        return year+"-"+month+"-"+day+" "+hour+":"+minutes+":"+seconds;
        
      }

    renderMessageFromStock(d){
        let data = JSON.parse(d.data);
        this.setState({socketMessaging: data.Message, socketMessaginToUpdate: data.Update});      
        if(data.Update) {

            this.atjjbChild.setWines();
            this.pendingChild.setWines();
        }         
    }
    
    render() {
        const token = this.props.token;
        const statsAtt = this.state.statsAtt || "";
        const statsPending = this.state.statsPending || "";

        if (token){
            return (
            <div>
                <Tab.Container id="tabs-with-dropdown" defaultActiveKey={1}>              
                <Row className="clearfix">
                  <Col sm={12}>
                    <Nav bsStyle="tabs" className="text-center">
                      <NavItem eventKey={1}>
                        <span>At JJbuckley <br/><small className="stats">{statsAtt}</small></span>
                      </NavItem>
                      <NavItem eventKey={2}>
                        <span>Pending Arrivals <br/><small className="stats">{statsPending}</small></span>
                      </NavItem>                
                    </Nav>
                  </Col>
                  <Col sm={12}>
                    <Tab.Content animation>
                      <Tab.Pane eventKey={1}>
                        <WinesJJB ref={instance => { this.atjjbChild = instance; }} nsId={this.props.user.CustomerId} onNoAuth={this.props.onNoAuth} setLegend={this.setTabLegend} setPhoto={this.props.setPhoto} />
                      </Tab.Pane>
                      <Tab.Pane eventKey={2}>
                        <WinesPending ref={instance => { this.pendingChild = instance; }} nsId={this.props.user.CustomerId} setLegend={this.setTabLegend} setPhoto={this.props.setPhoto} setOrder={this.props.setOrder} />
                      </Tab.Pane>                    
                    </Tab.Content>
                  </Col>
                </Row>
                </Tab.Container>

                <div> {this.state.socketMessaging + " " + this.getTIMESTAMP()}</div>
            </div>
            )
        } else {
            return (
                <p>loading</p>
            )
        }
    }
}

export default MyWines;