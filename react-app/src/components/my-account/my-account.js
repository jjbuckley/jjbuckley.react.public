import React, { Component } from 'react';

import MyWines from './wines/my-wines';

import Row from 'react-bootstrap/lib/Row';
import Col from 'react-bootstrap/lib/Col';
 
class MyAccount extends Component {

    render() {
        return (
            <Row className="show-grid">
                <Col md={12}>
                    <MyWines user={this.props.user} onNoAuth={this.props.onNoAuth} setTitle={this.props.setTitle} />
                </Col>
            </Row>
        );
    }
}
 
export default MyAccount;