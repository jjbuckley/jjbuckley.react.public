import React, { Component } from 'react';
import { Panel, Col, Row } from 'react-bootstrap/lib/';

class Overview extends Component{

    render(){
        return(
            <div>
                <Panel>
                    <Panel.Heading>Orders</Panel.Heading>
                    <Panel.Body>
                        <Row>
                            <Col sm={6}>
                                <p>View your order details, track your shipments and more.</p>
                            </Col>
                            <Col sm={6}>
                                <p>search bar</p>
                            </Col>
                        </Row>
                    </Panel.Body>
                    
                </Panel>

                <Panel>
                    <Panel.Heading>My Wines</Panel.Heading>
                    <Panel.Body>
                        <Row>
                            <Col sm={6}>
                                <p>View all your wine purchases. View your wines in storage and ready to ship. Ship your wines.</p>
                            </Col>
                            <Col sm={6}>
                                <p>stats</p>
                            </Col>
                        </Row>
                    </Panel.Body>
                </Panel>

                <Row>
                    <Col sm={6}>
                        <Panel>
                            <Panel.Heading>Profile</Panel.Heading>
                            <Panel.Body>
                                <p>Change your account settings (name, email and password). Manage your address book.</p>
                            </Panel.Body>
                        </Panel>
                    </Col>
                    <Col sm={6}>
                        <Panel>
                            <Panel.Heading>Payment Methods</Panel.Heading>
                            <Panel.Body>
                                <p>Update your credit card information. Add or remove credit cards.</p>
                            </Panel.Body>
                        </Panel>
                    </Col>
                </Row>
            </div>
        ) 
    }
}

export default Overview;