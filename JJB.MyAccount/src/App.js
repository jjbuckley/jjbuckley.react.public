import React, { Component } from 'react';
import Header from './components/layout/header';
import Body from './components/layout/body';
import Footer from './components/layout/footer';

import * as Api from './components/utils/ApiProxy';
import './App.css';
import Testimonal from './components/layout/testimonial/testimonial';

class App extends Component {
  constructor(props) {
    super(props);
    this.setUserData = this.setUserData.bind(this);
    this.invalidateUserData = this.invalidateUserData.bind(this);
    this.getToken = this.getToken.bind(this);
    this.checkToken = this.checkToken.bind(this);
    this.state = {
        user: JSON.parse(localStorage.getItem('api_user')),
        token: null,
    }
    console.log(process.env);
  }

  checkToken() {
    let token = false;
    let expireDate = parseInt(localStorage.getItem('api_expires_in'), 10);

    if (expireDate && expireDate > Date.now()) {
      token = localStorage.getItem('api_access_token');
      this.setState({token})
    }
    if(!token) {
      this.getToken();
    }
  }

  componentDidMount() {
    this.checkToken()    
  }

  getToken() {
    let apiUrl =  process.env.REACT_APP_JJBAPI_URL + 'token';
    let authData = {
        grant_type: 'password',
        username: process.env.REACT_APP_JJBAPI_USER_NAME,
        password: process.env.REACT_APP_JJBAPI_USER_PASSWORD
    }
    var formBody = [];
    for (let property in authData) {
        let encodedKey = encodeURIComponent(property);
        let encodedValue = encodeURIComponent(authData[property]);
        formBody.push(encodedKey + "=" + encodedValue);
    }
    formBody = formBody.join("&");

    fetch(apiUrl, {
        method: 'POST',
        mode: 'cors',
        body: formBody,
        headers: new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        })
    })
    .then(Api.handleResponse)
    .then(data => {
        console.log('setting token after request');
        console.log(data)
        localStorage.setItem('api_access_token', data.access_token);
        localStorage.setItem('api_expires_in', data.expires_in * 1000 + Date.now());
        localStorage.setItem('api_token_type', data.token_type);
        this.setState({token: data.access_token})
    })
    .catch(data => {
        throw data;
    })

  }

  setUserData(userObj) {
    localStorage.setItem('api_user', JSON.stringify(userObj));
    this.setState({
        user: userObj,
    })
  }
  
  invalidateUserData() {
    localStorage.removeItem('api_user');
    this.setState({
        user: null
    })
  }

  render() {
    const userData = this.state.user;
    const accessToken = this.state.token;
    return (
      <div className="app">
          <Header invalidateUserData={this.invalidateUserData} user={userData} token={accessToken}/>
          <Body setUserData={this.setUserData} invalidateUserData={this.invalidateUserData} user={userData} token={accessToken}/>
          <Testimonal token={accessToken}/>
          <Footer token={accessToken}/>
      </div>
    )
  }
}

export default App;
