import React, { Component } from 'react';
import * as Api from '../../utils/ApiProxy';
import { OverlayTrigger, Tooltip, Button } from 'react-bootstrap/lib/';

class WinesJJB extends Component {

  constructor(props){
    super(props);

    this.state = {
      wines: null
    }
    this.setWines = this.setWines.bind(this);
  }

  setWines(){
    const nsId = this.props.nsId;
    console.log("updating at JJB Wines " + nsId );
    Api.get('customers/' + nsId + '/atJJBWines')
    .then(data => {
      let wines = data ? data.map((item) => {
        return (
          <tr key={item.Sku}>
              <td>
                <OverlayTrigger placement="top" overlay={<Tooltip id={item.Sku}><img className="winePhoto" src={item.ImageFile} alt="bottle"/></Tooltip>}>
                  <Button bsStyle="link" className="camera" bsSize="lg" onClick={this.props.setPhoto.bind(this, item.WineName, item.ImageFile)}><span className="glyphicon glyphicon-camera"></span></Button>
                </OverlayTrigger>
              </td>
              <td>{item.Year}</td>
              <td>{item.WineName}</td>
              <td>{item.Varietal}</td>
              <td>{item.Size}</td>
              <td>{item.CurrentQty}</td>
              <td className="form-inline">
                <div className="form-group"><input type="checkbox"/> Qty <input type="number" max={item.CurrentQty} min="0" className="form-control input-sm"/></div>
              </td>
          </tr>
        )
      }) : <tr><td colspan="6"></td></tr>;
      
      let winesCount = data.length !== 'undefined' ? data.length : 0;
      let bottlesCount = 0;
      data.forEach(wine => {
        bottlesCount += wine.CurrentQty;
      });

      let legend = winesCount + (winesCount === 1 ? " wine" : " wines") + ", " + bottlesCount + (bottlesCount === 1 ? " bottle" : " bottles")

      this.props.setLegend(1, legend);
      this.setState({wines});
      console.log(nsId + " At JJB updated");
      
    })
    .catch(err => {
      console.log(err)
      let issues = (
        <tr>
          <td colSpan={5}>We are experiencing issues with the server connection...</td>
        </tr>
      )
      this.setState({wines: issues});      
    })
  }
  
  componentDidMount() {
    this.setWines();
  }

  render() {
    const wineList = this.state.wines;
    return (
      <div>
        {wineList ?
        <div className="table-responsive">
            <table className="table table-hover">
                <thead>
                    <tr>
                      <th></th>
                      <th>Vintage</th>
                      <th>Wine</th>
                      <th>Varietal</th>
                      <th>Size</th>
                      <th>Qty</th>
                      <th></th>
                    </tr>
                </thead>
                <tbody>
                  {wineList}
                </tbody>
            </table>
          </div>
          :
          <div className="text-center">
            <div className="loader-circle-head big gray" id="spinner">
            </div>
        </div>
        }
      </div>
    );
  }
}

export default WinesJJB;
