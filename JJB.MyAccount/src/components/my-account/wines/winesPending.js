import React, { Component } from 'react';
import * as Api from '../../utils/ApiProxy';
import { OverlayTrigger, Tooltip, Button } from 'react-bootstrap/lib/';

class WinesPending extends Component {
  constructor(props){
    super(props);
    this.state = {
      wines: null
    }
    this.setWines = this.setWines.bind(this);
  }

  componentDidMount() {
    this.setWines();
  }

  setWines() {
    const nsId = this.props.nsId;
    console.log("updating at Pending Arrival Wines " + nsId );
    Api.get('customers/' + nsId + '/pendingJJBWines')
    .then(data => {
      let wines = data.Items ? data.Items.map((item) => {
        return (
          <tr key={item.Sku}>
              <td>
                <OverlayTrigger placement="top" overlay={<Tooltip id={item.Sku}><img className="winePhoto" src={item.ImageFile} alt="bottle"/></Tooltip>}>
                  <Button bsStyle="link" className="camera" bsSize="lg" onClick={this.props.setPhoto.bind(this, item.WineName, item.ImageFile)}><span className="glyphicon glyphicon-camera"></span></Button>
                </OverlayTrigger>
              </td>
              <td>{item.Year}</td>
              <td>{item.WineName}</td>
              <td>{item.Varietal}</td>
              <td>{item.Size}</td>
              <td>{item.Quantity}</td>
              <td>
                <ul className="list-unstyled">
                  {item.Orders.map((order) => {
                    return (
                    <li key={order.TranId}><a href={"#" + order.NsInternalTranId} onClick={this.props.setOrder.bind(this, order.NsInternalTranId)}>{order.TranId}</a></li>
                  )})}
                </ul>
              </td>
              <td>{item.ShipDate}</td>
              <th className="hidden">{item.RawShipDate}</th>
          </tr>
        )
      }) : <tr><td colSpan="8"></td></tr>;

      const winesCount = data.Wines;
      const bottlesCount = data.Bottles;
      let legend = winesCount + (winesCount === 1 ? " wine" : " wines") + ", " + bottlesCount + (bottlesCount === 1 ? " bottle" : " bottles")

      console.log(nsId + " pending arrival updated");
      this.props.setLegend(2, legend);
      this.setState({wines: wines});
    })
    .catch(err => {
      console.log(err)
      let issues = (
        <tr>
          <td colSpan={7}>We are experiencing issues with the server conection...</td>
        </tr>
      )
      this.setState({wines: issues});
    })
  }

  render() {
    const wineList = this.state.wines;
    return (
      <div>
      {wineList ? 
      <div className="table-responsive">
          <table className="table table-hover">
              <thead>
                  <tr>
                      <th></th>
                      <th>Vintage</th>
                      <th>Wine</th>
                      <th>Varietal</th>
                      <th>Size</th>
                      <th>Qty</th>
                      <th>Order</th>
                      <th>ETA</th>
                      <th className="hidden">Raw Ship Code</th>
                  </tr>
              </thead>
            <tbody>
              {wineList}
            </tbody>
          </table>
        </div>
        :
        <div className="text-center">
            <div className="loader-circle-head big gray" id="spinner">
            </div>
        </div>
      }
      </div>
    );
  }
}

export default WinesPending;
