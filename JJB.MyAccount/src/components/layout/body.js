import React, { Component } from 'react';

import { Grid, Row, Image } from 'react-bootstrap/lib/';
import MyAccountPage from "../my-account/my-account";
import Login from "../my-account/login";

import './body.css';
import MainBg from '../../assets/main-bg.jpg';

class Body extends Component {
    constructor(props) {
        super(props);
        this.setTitle = this.setTitle.bind(this);
        this.state = {
            title: 'hello'
        }
    }
    
    setTitle(title) {
        this.setState({
            title: title
        })
    }

    render() {
        const user = this.props.user;
        const title = this.state.title;
        return (
            <div className='body-holder'>          
                <div className='main-bg'><Image src={MainBg} className='pull-right' /></div>
                <Grid>
                    <Row className='bheader'>
                        <h2>{title}</h2>
                    </Row>
                </Grid>
                <Grid className='body'>
                    <div>testing form beta</div>
                    <div className='bcontent'>
                    {user ?
                        <MyAccountPage user={user} onNoAuth={this.props.invalidateUserData} setTitle={this.setTitle} token={this.props.token} />
                        :
                        <Login onSignin={this.props.setUserData} setTitle={this.setTitle}/>
                    }
                    </div>
                </Grid>
                <div className='bfooter'>
                </div>
            </div>
        );
    }
}
 
export default Body;
