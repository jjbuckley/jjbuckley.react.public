import React, { Component } from 'react';
import * as Api from '../../utils/ApiProxy';

class WinesPending extends Component {
  constructor(props){
    super(props);
    this.state = {
      wines: []
    }
  }

  componentDidMount() {
    const nsId = this.props.nsId;
    Api.get('customers/' + nsId + '/pendingJJBWines')
    .then(data => {
      let wines = data.Items.map((item) => {
        return (
          <tr key={item.Sku}>
              <td>{item.Year}</td>
              <td>{item.WineName}</td>
              <td>{item.Varietal}</td>
              <td>{item.Size}</td>
              <td>{item.Quantity}</td>
              <td>
                  <div>
                      order ids
                  </div>
              </td>
              <td>{item.ShipDate}</td>
          </tr>
        )
      });
      
      this.setState({wines: wines});
    })
  }

  render() {
    return (
          <table className="table table-striped table-hover table-condensed">
              <thead>
                  <tr>
                      <th>Vintage</th>
                      <th>Wine</th>
                      <th>Varietal</th>
                      <th>Size</th>
                      <th>Qty</th>
                      <th>Order</th>
                      <th>ETA</th>
                  </tr>
              </thead>

            <tbody>
              {this.state.wines}
            </tbody>
          </table>
    );
  }
}

export default WinesPending;
