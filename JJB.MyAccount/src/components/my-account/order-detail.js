import React, { Component } from 'react';
import * as Api from '../utils/ApiProxy';

import {Col, Button, Modal, OverlayTrigger, Tooltip, FormGroup, FormControl, ControlLabel, Form} from 'react-bootstrap/lib/';
 
class OrderDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalOrder: null,
            showStatus: false,
            showInquiry: false,
            validInputs: {
                emailFrom: null,
                firstName: null,
                lastName: null,
                message: null,
                subject : null
            }
        }
        this.showStatusModal = this.showStatusModal.bind(this);
        this.hideStatusModal = this.hideStatusModal.bind(this);
        this.showInquiryModal = this.showInquiryModal.bind(this);
        this.hideInquiryModal = this.hideInquiryModal.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.sendInquiry = this.sendInquiry.bind(this);
    }

    handleInputChange (event){
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        let validations = this.state.validInputs;
        validations[name] = null;

        this.setState({
            [name]: value,
            validInputs: validations,
        });
    }

    formatCurrency(value) {
        if (value != null && value >= 0) {
            return "$" + parseFloat(isNaN(value) ? 0 : value).toFixed(2);
        } else {
            return "";
        }
    }

    formatDate(text) {
        var value = new Date(text);
        var options = { year: 'numeric', month: 'short', day: 'numeric' };

        if (value.getFullYear() >= 2014) {
            return value.toLocaleDateString("en-US", options); // Sep 17, 2016
        } else {
            return "";
        }
    }

    formatDateShort(text) {
        var date = new Date(text),
            response = "";

        if (date.getFullYear() >= 2014) {
            response = date.toLocaleDateString("en-US"); // 9/17/2016
        }

        return response;
    }

    getStatus(item, shippingMethod) {
        if (item.TrackingLinks === null || item.TrackingLinks.length === 0) {
            if (item.Status === "Pending Arrival") {
                return item.Status;
            } else if (item.Status === "Shipped" && shippingMethod.indexOf("STORAGE") > -1) {
                return "Arrived"
            }
            else if (item.Status === "Just Arrived" || item.Status === "JJB Storage")
                return "Arrived"
            else
                return item.Status;
        }
        else {
            return "Shipped";
        }
    }

    getTracking(shippingMethod, item) {
        var response = "N/A";

        if (!item.TrackingLinks || item.TrackingLinks.length === 0) {
            if (item.Status === "Shipped") {
                response = "";
                if (shippingMethod.indexOf("CUSTOMER") > -1) {
                    response = "Customer Pick-up";
                }
                if (shippingMethod.indexOf("FREIGHT FORWARD") > -1) {
                    response = "Freight Forward - Carrier does not supply tracking info.";
                }
                if (shippingMethod.indexOf("VIN-GO") > -1) {
                    response = "Vin-Go - Carrier does not supply tracking info.";
                }
                if (shippingMethod.indexOf("STORAGE") > -1) {
                    response = "At JJ Buckley";
                }
            }
            else if (item.Status === "Arrived" || item.Status === "Just Arrived") {
                response = "At JJ Buckley";
            }
        }
        else
            response = "";

        return response;
    }

    getCleanedShipperName(shippingMethod) {
        var response = "",
            cleanName = function (method) {
                var response = method,
                    index = method.indexOf(" OAK");
                if (index > -1) {
                    response = method.substr(0, index);
                }
                return response;
            };

        if (shippingMethod) {
            shippingMethod = shippingMethod.toUpperCase();

            if (shippingMethod.indexOf('FEDEX') > -1) {
                if (shippingMethod === 'FEDEX 2-DAY AM') {
                    response = 'FEDEX 2-DAY';
                } else {
                    response = cleanName(shippingMethod);
                }
            }

            if (shippingMethod.indexOf('UPS') > -1) {
                if (shippingMethod === 'UPS NEXT DAY AIR EARLY A.M. CA') {
                    response = 'UPS NEXT DAY AIR EARLY A.M.';
                } else {
                    response = cleanName(shippingMethod);
                }
            }

            if (shippingMethod === 'STORAGE OAK') {
                response = 'JJB STORAGE';
            }
            if (shippingMethod === 'FREIGHT FORWARD OAK') {
                response = 'FREIGHT FORWARD';
            }
            if (shippingMethod === 'VIN-GO') {
                response = 'VIN-GO';
            }

            if (shippingMethod.indexOf('GSO') > -1) {
                response = cleanName(shippingMethod);
            }

            if (shippingMethod.indexOf('CUSTOMER') > -1) {
                response = cleanName(shippingMethod);
            }

            response = response || shippingMethod;

        }
        return response;
    }

    showStatusModal(e) {
        e.preventDefault()
        this.setState({showStatus: true})
    }

    hideStatusModal() {
        this.setState({showStatus: false})
    }

    showInquiryModal(orderID, emailTo, subject) {
        this.setState({
            showInquiry: true,
            orderID,
            emailTo,
            subject
        })
    }

    hideInquiryModal() {
        this.setState({showInquiry: false})
    }

    sendInquiry(orderID, nsOrderNumber) {
        const {firstName, lastName, emailFrom, emailTo, subject, message} = this.state;
        const body = {
            orderID,
            username: firstName + " " + lastName,
            emailFrom: emailFrom,
            emailTo: emailTo,
            subject: subject,
            message: message
        }
        const validInputs = {
            firstName: !firstName || firstName === '' ? 'error' : null,
            lastName: !lastName || lastName === '' ? 'error' : null,
            emailFrom: !emailFrom || emailFrom === '' ? 'error' : null,
            subject: !subject || subject === '' ? 'error' : null,
            message: !message || message === '' ? 'error' : null
        }
        const isOk = (firstName && firstName !== '') && (lastName && lastName !== '') && (emailFrom && emailFrom !== '') && (subject && subject !== '') && (message && message !== '');
        
        if (isOk) {
            // send inquiry
            Api.post('customers/' + this.props.customerId + '/orders/' + nsOrderNumber + "/inquiry", body)
            .then(data => {
                this.setState({
                    showInquiry: false
                })
            })
            .catch(err => {
                console.log(err)
            })
            
        }
        this.setState({validInputs});
        
    }

    render() {
        const showOrder = this.props.showOrder;
        const showStatus = this.state.showStatus;
        const showInquiry = this.state.showInquiry;
        const modalOrderImages = this.props.modalOrderImages || [];
        const validInputs = this.state.validInputs;
        let modalOrder = this.props.modalOrder;
        let imgData;

        if (modalOrder && modalOrder.Items) {
            for (let item of modalOrder.Items) {
                imgData = modalOrderImages.find(img => { return img.SKU === item.Sku });
                item.ImageFile = imgData ? imgData.ImageUrl : ""
                item.ExpectedArrivalDate = imgData ? imgData.ExpectedArrivalDate : null;
            }
        }
        
        return (
            <div>
                <Modal show={showOrder} onHide={this.props.hideOrder} bsSize="large" >
                    <Modal.Header closeButton>
                        <Modal.Title>{modalOrder ? "Order #" + modalOrder.OrderId : "Loading"}</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        {modalOrder ?
                        <div>
                        <Col md={12}>
                            <ul className="list-unstyled">
                                <li><strong>Ordered on:</strong> { this.formatDate(modalOrder.Date) }</li>
                                <li>
                                    <strong>Billed To:</strong> { modalOrder.Billing }
                                </li>
                                {modalOrder.PaymentInfo && modalOrder.PaymentInfo.PaymentMethod ? 
                                <li>
                                    <strong>Paid By:</strong> { modalOrder.PaymentInfo.PaymentMethod }{ modalOrder.PaymentInfo.CCNumber && modalOrder.PaymentInfo.CCNumber !== "" ? ", " + modalOrder.PaymentInfo.CCNumber + ", " + modalOrder.PaymentInfo.CCName : ""}
                                </li>
                                : ""}
                            </ul>

                            <Button className="btn-inq" bsStyle="default" onClick={ this.showInquiryModal.bind(this, modalOrder.OrderId, modalOrder.SalesEmail, "I have a question about my order #" + modalOrder.OrderId) }>Order Inquiry</Button>

                        </Col>
                        <Col md={12}>
                        <table className="table">
                            <thead>
                                <tr>
                                    <th className="hidden-xs"></th>
                                    <th>Wine<span className="hidden-xs"> Name</span></th>
                                    <th>Price</th>
                                    <th>Qty</th>
                                    <th>Extended</th>
                                    <th>
                                        Status
                                        <a href="#statusModal" onClick={this.showStatusModal}><span className="glyphicon glyphicon-info-sign"></span></a>
                                    </th>
                                    <th>Tracking<span className="hidden-xs">/Location</span></th>
                                </tr>
                            </thead>
                            <tbody>
                                { modalOrder.Items.map((item) => {
                                    const status = this.getStatus(item, modalOrder.ShippingMethod);
                                    return(
                                <tr key={item.ItemId}>
                                    <td className="text-center">
                                        {item.ImageFile ? 
                                        <OverlayTrigger placement="top" overlay={<Tooltip id={item.Sku}><img className="winePhoto" src={item.ImageFile || ""} alt="bottle"/></Tooltip>}>
                                            <Button bsStyle="link" className="camera" bsSize="lg" onClick={this.props.setPhoto.bind(this, item.Item, item.ImageFile|| "")}><span className="glyphicon glyphicon-camera"></span></Button>
                                        </OverlayTrigger>
                                        : 
                                        <span className="glyphicon glyphicon-camera text-muted"></span>
                                        }
                                    </td>
                                    <td>{ item.Item }</td>
                                    <td className="text-right">{ this.formatCurrency(item.Rate) }</td>
                                    <td className="text-right">{ item.Quantity }</td>
                                    <td className="text-right">{ this.formatCurrency(item.Extended) }</td>
                                    <td>
                                        { status }
                                        { item.ExpectedArrivalDate && status === "Pending Arrival" ? <p className='date' data-bottle-id={item.Sku}>ETA - {item.ExpectedArrivalDate}</p> : ""}</td>
                                    <td>
                                        { this.getTracking(modalOrder.ShippingMethod, item) }
                                        
                                        { item.TrackingInfo.map((info) => {
                                            const trackingNumber = info.TrackingNumber;
                                            return(
                                                trackingNumber !== "nocarrier" && trackingNumber !== "n/a" ?
                                                    <a key={item.ItemId} href={ info.TrackingLinks } data-toggle="tooltip" title={ this.getCleanedShipperName(info.ItemShipMethod) + " " + this.formatDateShort(info.TrackDate) } target="_blank">{ info.TrackingNumber }</a>
                                                : ""
                                            )}
                                        )}

                                    </td> 
                                </tr>
                                )})
                                }
                                <tr>
                                    <td className="hidden-xs"></td>
                                    <td><span className="badge">{ modalOrder.WinesQuantity } wines</span></td>
                                    <td colSpan="2" className="text-right"><span className="badge">{ modalOrder.BottlesQuantity } bottles</span></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr className="text-right">
                                    <td className="hidden-xs"></td>
                                    <td></td>
                                    <td colSpan="2" className="active">Subtotal</td>
                                    <td>{ this.formatCurrency(modalOrder.SubTotal) }</td>
                                    <td colSpan="2"></td>
                                </tr>
                                <tr className="text-right">
                                    <td className="hidden-xs"></td>
                                    <td></td>
                                    <td colSpan="2" className="active">Sales Tax</td>
                                    <td>{ this.formatCurrency(modalOrder.TaxTotal) }</td>
                                    <td colSpan="2"></td>
                                </tr>
                                <tr className="text-right">
                                    <td className="hidden-xs"></td>
                                    <td></td>
                                    <td colSpan="2" className="active">
                                        Shipping
                                        <OverlayTrigger placement="top" overlay={<Tooltip id="shipping-tooltip">Reflects shipping costs paid on this order. For items sent to storage and shipped later, please refer to the order number associated with the shipment for shipping costs and tracking information.</Tooltip>}>
                                            <span className="glyphicon glyphicon-question-sign"></span>
                                        </OverlayTrigger>
                                    </td>
                                    <td>{ this.formatCurrency(modalOrder.ShippingCost) }</td>
                                    <td colSpan="2"></td>
                                </tr>
                                <tr className="text-right">
                                    <td className="hidden-xs"></td>
                                    <td></td>
                                    <td colSpan="2" className="active">Insurance</td>
                                    <td>{ this.formatCurrency(modalOrder.Insurance) }</td>
                                    <td colSpan="2"></td>
                                </tr>

                                <tr className="grandtotal text-right">
                                    <td className="hidden-xs"></td>
                                    <td></td>
                                    <td colSpan="2" className="active">ORDER TOTAL</td>
                                    <td>{ this.formatCurrency(modalOrder.Total) }</td>
                                    <td colSpan="2"></td>
                                </tr>

                            </tfoot>
                        </table>
                        </Col>
                        </div>
                        : 
                        <div className="text-center">
                            <div className="loader-circle-head big gray" id="spinner">
                            </div>
                        </div>
                        }
                        <div className="clearfix"></div>
                        </Modal.Body>

                    <Modal.Footer>
                        <Button onClick={this.props.hideOrder}>Close</Button>
                    </Modal.Footer>
                </Modal>

                <Modal show={showStatus} onHide={this.hideStatusModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>                
                            Order Items Status
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <dl className="dl-horizontal">
                            <dt>Pending Arrival</dt>
                            <dd>Item is back-ordered. You will be notified when this item arrives.</dd>

                            <dt>Arrived</dt>
                            <dd>
                                Item is at JJ Buckley and available for shipment or pick-up. Visit <a href="/account/my-wines">My Wines</a> to create a shipment.
                            </dd>

                            <dt>Shipped</dt>
                            <dd>Item has shipped. Click the tracking link to view the shipment on the carrier website.<strong>**</strong></dd>
                        </dl>

                        <p>
                            <strong>*</strong>For items sent to storage and shipped later, please refer to the order number associated with the shipment for shipping costs and tracking information.
                        </p>
                        <p>
                            <strong>**</strong>For Shipped items without a visible "Track" link, please contact us for information about your shipment.
                        </p>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={this.hideStatusModal}>Close</Button>
                    </Modal.Footer>
                </Modal>

                <Modal show={showInquiry} onHide={this.hideInquiryModal}>
                    <Modal.Header closeButton>
                        <Modal.Title>                
                            Order Inquiry
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form>
                        <FormGroup validationState={validInputs.firstName}>
                            <ControlLabel>First Name</ControlLabel>
                            <FormControl type="text" name="firstName" onChange={this.handleInputChange} required />
                        </FormGroup>
                        <FormGroup validationState={validInputs.lastName}>
                            <ControlLabel>Last Name</ControlLabel>
                            <FormControl type="text" name="lastName" onChange={this.handleInputChange} required />
                        </FormGroup>
                        <FormGroup validationState={validInputs.emailFrom}>
                            <ControlLabel>From</ControlLabel>
                            <FormControl type="email" name="emailFrom" onChange={this.handleInputChange} required />
                        </FormGroup>
                        <FormGroup validationState={validInputs.emailTo}>
                            <ControlLabel>To</ControlLabel>
                            <FormControl type="email" name="emailTo" onChange={this.handleInputChange} disabled value={modalOrder ? modalOrder.SalesEmail : ""} />
                        </FormGroup>
                        <FormGroup validationState={validInputs.subject}>
                            <ControlLabel>Subject</ControlLabel>
                            <FormControl type="text" name="subject" onChange={this.handleInputChange} value={this.state.subject} required />
                        </FormGroup>
                        <FormGroup validationState={validInputs.message}>
                            <ControlLabel>Message</ControlLabel>
                            <FormControl componentClass="textarea" name="message" onChange={this.handleInputChange} required />
                        </FormGroup>
                        </Form>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button bsStyle="link" onClick={this.hideInquiryModal}>Close</Button>
                        <Button bsStyle="primary" onClick={this.sendInquiry.bind(this, modalOrder ? modalOrder.OrderId : "00", modalOrder ? modalOrder.Id : "00")}>Send</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}
 
export default OrderDetail;