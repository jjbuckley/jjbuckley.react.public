import React, { Component } from 'react';
import {Col, MenuItem, NavDropdown} from 'react-bootstrap/lib/';
//import * as Api from '../../utils/ApiProxy';

// import './menu.css';

class MenuChildren extends Component {

    render() {
        const item = this.props.item;
        const hasChilds = item.children && item.children.length > 0;
        let childLength = 0;
        let colWidth = 0;

        if (hasChilds) {
            item.children.forEach(element => {
                if (element.children && element.children.length >= 1) {
                    childLength += 1;
                }
            });
            colWidth = Math.floor(12/childLength);
        }

        return (
            <NavDropdown title={item.title} id={item.title} className={'hasCols-'+childLength} href={item.friendlyUrl}>
                {hasChilds ? 
                    item.children.map((subitem) => {
                        return(subitem.children && subitem.children.length >= 1 ?
                            <Col sm={colWidth} key={subitem.title}>
                                <MenuItem header>{subitem.title}</MenuItem>
                                {subitem.children.map((subsubitem) => {
                                    return(<MenuItem key={subsubitem.url} href={subsubitem.friendlyUrl}>{subsubitem.title}</MenuItem>)
                                })}
                            </Col>
                        :
                            <MenuItem key={subitem.title} href={subitem.friendlyUrl}>{subitem.title}</MenuItem>
                        )
                    }) 
                : null}
            </NavDropdown>
        );
    }
}
             
export default MenuChildren;