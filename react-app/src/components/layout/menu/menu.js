import React, { Component } from 'react';
import {Navbar, Nav, NavItem} from 'react-bootstrap/lib/';
import MenuChildren from './menuChildren';
import './menu.css';

class Menu extends Component {
    componentDidMount() {
        // $('nav.topMenu .navbar-nav>li>a').on('hover', function(){
        //     let $link = $(this);
        //     $link.next().css('display', 'block');
        // })
    }
    render() {
        const items = this.props.items;
        return (
            <Navbar inverse collapseOnSelect className="topMenu">
                <Navbar.Header>
                    <Navbar.Toggle />
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav>
                        {items.map((item) => {
                            let isText = item.title === '|';
                            
                            return ( isText ? 
                                <NavItem disabled key={item.title}>{item.title}</NavItem>
                                :
                                <MenuChildren item={item} key={item.title}/>
                            )
                        })}
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }
    
}
             
export default Menu;