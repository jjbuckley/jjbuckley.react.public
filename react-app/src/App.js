import React, { Component } from 'react';
import Header from './components/layout/header';
import Body from './components/layout/body';
import Footer from './components/layout/footer';

import './App.css';
import Testimonal from './components/layout/testimonial/testimonial';

class App extends Component {
  constructor(props) {
    super(props);
    this.setUserData = this.setUserData.bind(this);
    this.invalidateUserData = this.invalidateUserData.bind(this);
    this.state = {
        user: JSON.parse(localStorage.getItem('api_user'))
    }

    console.log(process.env);
  }

  setUserData(userObj) {
    localStorage.setItem('api_user', JSON.stringify(userObj));
      
      this.setState({
          user: userObj,
      })
  }
  invalidateUserData() {
      this.setState({
          user: null
      })
  }

  render() {
    const userData = this.state.user;
    return (
      <div className="app">
          <Header invalidateUserData={this.invalidateUserData} user={userData}/>
          <Body setUserData={this.setUserData} invalidateUserData={this.invalidateUserData} user={userData}/>
          <Testimonal/>
          <Footer/>
      </div>
    )
  }
}

export default App;
