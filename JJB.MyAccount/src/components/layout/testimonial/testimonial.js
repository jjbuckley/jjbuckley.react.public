import React, { Component } from 'react';
import {Jumbotron, Grid} from 'react-bootstrap/lib/';
import * as Api from '../../utils/ApiProxy';

import './testimonial.css';

class Testimonal extends Component {

    constructor(props) {
        super(props);
        this.state = {
            testimonal: null
        }
    }

    fetchData() {
        if (this.props.token && !this.state.testimonal) {
            Api.get('testimonials/random')
            .then(data => {
                this.setState({testimonal: data});
            })
            .catch(data => {
                console.error("Can't get testimonial", data);
            })
        }
    }

    componentDidMount() {
        this.fetchData();
    }

    render() {
        if(this.props.token) this.fetchData();
        const testimonal = this.state.testimonal;
        return (
            <div className="testimonial">
            {testimonal ? 
            <Grid>
            <Jumbotron>
                <span className="quote">&#8220;</span>
                <p>{testimonal.Quote}</p>
                <p className="author"><small>-{testimonal.Name}, {testimonal.DateSaid}</small></p>
            </Jumbotron>
            </Grid>
            :null
            }
            </div>
        )
    }
}

export default Testimonal