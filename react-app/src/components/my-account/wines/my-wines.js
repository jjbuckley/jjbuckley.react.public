import React, { Component } from 'react';
import WinesJJB from "./winesJJB.js";
import WinesPending from "./winesPending.js";
import WinesShipping from "./winesShipping.js";
import Tabs from 'react-bootstrap/lib/Tabs';
import Tab from 'react-bootstrap/lib/Tab';
import * as Api from '../../utils/ApiProxy';

class MyWines extends Component {
    constructor(props) {
        super(props);
        this.state = {
            token: null
        }
    }

    componentDidMount() {
        let token = Api.checkToken();
        if(token){
            this.setState({
                token: token
            });
        }
        
        this.props.setTitle('My Wines')
    }
    
    render() {
        if (this.state.token){
            return (
                <Tabs defaultActiveKey={2} id="uncontrolled-tab-example">
                    <Tab eventKey={1} title="At JJbuckley">
                        <WinesJJB nsId={this.props.user.CustomerId} onNoAuth={this.props.onNoAuth} />
                    </Tab>
                    <Tab eventKey={2} title="Pending Arrivals">
                        <WinesPending nsId={this.props.user.CustomerId} />
                    </Tab>
                    <Tab eventKey={3} title="Pending Shipments">
                        <WinesShipping nsId={this.props.user.CustomerId}/>
                    </Tab>
                </Tabs>
            )
        } else {
            return (
                <p>loading</p>
            )
        }
    }
}

export default MyWines;