import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom'

import MyWines from './wines/my-wines';
import Overview from './wines/overview';
import Orders from './wines/orders';

import { Col, Row, Nav, NavItem, Modal, Button } from 'react-bootstrap/lib/';
import { LinkContainer } from "react-router-bootstrap";
import './my-account.css'
import SocketTest from './wines/websocket';

import * as Api from '../utils/ApiProxy';
import OrderDetail from './order-detail.js';

class MyAccount extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showOrder: false,
            modalPhoto: null,
            modalTitle: null,
            showPhoto: false,
        };
        this.setShowModalOrder = this.setShowModalOrder.bind(this);
        this.hideModalOrder = this.hideModalOrder.bind(this);
        this.setShowModalPhoto = this.setShowModalPhoto.bind(this);
        this.hideModalPhoto = this.hideModalPhoto.bind(this);
    }

    setShowModalPhoto(title, url) {
        this.setState({
            modalPhoto: url, 
            modalTitle: title,
            showPhoto: true
        })
    }

    hideModalPhoto() {
        this.setState({showPhoto: false})
    }

    setShowModalOrder(nsOrderNumber, e) {
        this.setState({
            showOrder: true
        });

        e.preventDefault(); 

        //get order info
        Api.get('customers/' + this.props.user.CustomerId + '/orders/' + nsOrderNumber)
        .then(data => {
            this.setState({
                modalOrder: data
            })
        })
        .catch(err => {
            console.log(err)
        })

        //get order wine images
        Api.get('customers/' + this.props.user.CustomerId + '/orders/' + nsOrderNumber)
        .then(data => {
            this.setState({
                modalOrder: data
            })

            let skus = [];
            data.Items.forEach(item => {
                skus.push(item.Sku);
            });

            Api.post("wines/images", skus)
            .then(imgData => {
                this.setState({
                    modalOrderImages: imgData
                })
            });

        })
        .catch(err => {
            console.log(err)
        })
    }

    hideModalOrder() {
        this.setState({showOrder: false, modalOrder: null})
    }

    render() {
        const showOrder = this.state.showOrder;
        const modalOrder = this.state.modalOrder;
        const modalOrderImages = this.state.modalOrderImages;

        const winePhoto = this.state.modalPhoto;
        const wineTitle = this.state.modalTitle;
        const showPhoto = this.state.showPhoto;

        return (
            <Row className="show-grid my-account">
                <Col md={12}>
                    <Nav bsStyle="pills">
                        <LinkContainer to="/account" exact>
                            <NavItem>Overview</NavItem>
                        </LinkContainer>                      
                        <LinkContainer to="/account/orders" exact>
                            <NavItem>Orders</NavItem>
                        </LinkContainer>
                        <LinkContainer to="/account/my-wines" exact>
                            <NavItem>My Wines</NavItem>
                        </LinkContainer>
                        <LinkContainer to="/account/profile" exact>
                            <NavItem disabled>Profile</NavItem>
                        </LinkContainer>
                        <LinkContainer to="/account/payment" exact>
                            <NavItem disabled>Payment Methods</NavItem>
                        </LinkContainer> 
                        <LinkContainer to="account/websocket">
                            <NavItem>Socket</NavItem>
                        </LinkContainer>                   
                    </Nav>
                </Col>
                <Col md={12}>
                    <Switch>
                        <Route exact path="/account" component={Overview} />
                        <Route exact path="/account/websocket" component={SocketTest}/>
                        <Route exact path="/account/my-wines">
                            <MyWines user={this.props.user} onNoAuth={this.props.onNoAuth} setTitle={this.props.setTitle} token={this.props.token} setPhoto={this.setShowModalPhoto} setOrder={this.setShowModalOrder} />
                        </Route>
                        <Route exact path="/account/orders">
                            <Orders user={this.props.user} onNoAuth={this.props.onNoAuth} setTitle={this.props.setTitle} token={this.props.token} setPhoto={this.setShowModalPhoto} setOrder={this.setShowModalOrder} />
                        </Route>
                    </Switch>
                </Col>

                <Modal show={showPhoto} onHide={this.hideModalPhoto} bsSize="small">
                    <Modal.Header closeButton>
                        <Modal.Title>{wineTitle}</Modal.Title>
                    </Modal.Header>

                    <Modal.Body className="text-center">
                        <img src={winePhoto} alt="wine bottle"/>
                    </Modal.Body>

                    <Modal.Footer>
                        <Button onClick={this.hideModalPhoto}>Close</Button>
                    </Modal.Footer>
                </Modal>

                <OrderDetail customerId={this.props.user.CustomerId} showOrder={showOrder} setPhoto={this.setShowModalPhoto} hideOrder={this.hideModalOrder} modalOrder={modalOrder} modalOrderImages={modalOrderImages} />

            </Row>
        );
    }
}
 
export default MyAccount;