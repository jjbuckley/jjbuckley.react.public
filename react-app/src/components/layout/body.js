import React, { Component } from 'react';
import {Route, Switch } from 'react-router-dom'

import { Grid, Row, Image } from 'react-bootstrap/lib/';
import MyAccountPage from "../my-account/my-account";
import Login from "../my-account/login";
import MyAccountOverview from '../my-account/wines/overview';

import './body.css';
import MainBg from '../../assets/main-bg.jpg';

class Body extends Component {
    constructor(props) {
        super(props);
        this.setTitle = this.setTitle.bind(this);
        this.state = {
            title: 'hello'
        }

        console.log(process.env);
    }
    
    setTitle(title) {
        this.setState({
            title: title
        })
    }
    render() {
        const user = this.props.user;
        const title = this.state.title;
        return (
            <div className='body-holder'>
                <div className='main-bg'><Image src={MainBg} className='pull-right' /></div>
                <Grid>
                    <Row className='bheader'>
                    <h2>{title}</h2>
                    </Row>
                </Grid>
                <Grid className='body'>
                    <div> Hola test </div> 
                    <div className='bcontent'>
                        <Switch>
                            <Route exact path="/" component= { MyAccountOverview } />
                            <Route path="/my-wines">
                                {user ? <MyAccountPage user={user} onNoAuth={this.props.invalidateUserData} setTitle={this.setTitle} />: <Login onSignin={this.props.setUserData} setTitle={this.setTitle}/>}
                            </Route>
                        </Switch>
                    </div>
                </Grid>
                <div className='bfooter'>
                </div>
            </div>
        );
    }
}
 
export default Body;
