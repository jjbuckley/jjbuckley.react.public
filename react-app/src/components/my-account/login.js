import React, { Component } from 'react';
import * as Api from '../utils/ApiProxy';

import {Row, Col, Button, FormGroup, ControlLabel, FormControl, Alert} from 'react-bootstrap/lib/';
 
class MyAccount extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isError: false
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.authenticate = this.authenticate.bind(this);
    }

    componentDidMount () {
        this.props.setTitle('Sign in')
    }

    handleInputChange (event){
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });

    }

    authenticate (){
        Api.post('customers/auth', {email: this.state.email, password: this.state.password}, null)
            .then(data => {
                this.setState({isError: false});
                this.props.onSignin(data);
            })
            .catch(data => {
                this.setState({isError: true});
                console.error('no auth', data);
            })
    }

    render() {
        const isError = this.state.isError;
        return (
            <Row className="show-grid">
                <Col sm={12}>
                    <p>By signing in, you can access your past orders, view your account information, set up shipments and more.</p>
                </Col>
                <Col md={4} mdOffset={1} sm={6}>
                    {isError ?
                    <Alert bsStyle="danger">
                        The <strong>email/password</strong> is not correct.<br/>
                        Please verify and try again or visit the <a href="/forgot-password" className="alert-link">Forgot Password</a> page and follow the password reset instructions.
                    </Alert>
                    :null
                    }
                    <form>
                        <FormGroup controlId="txt-Email">
                        <ControlLabel>Email</ControlLabel>
                            <FormControl type="email" name="email" onChange={this.handleInputChange} required />
                        </FormGroup>

                        <FormGroup controlId="txt-Password" >
                            <ControlLabel>Password</ControlLabel>
                            <FormControl type="password" name="password" onChange={this.handleInputChange} required />
                        </FormGroup>

                        <Button bsStyle="primary" block onClick={this.authenticate}>Login</Button>
                        
                    </form>
                </Col>
                <Col md={4} mdOffset={1} sm={6}>
                    <p className='text-center'>Facebook placeholder</p>
                </Col>
                
            </Row>
        );
    }
}
 
export default MyAccount;