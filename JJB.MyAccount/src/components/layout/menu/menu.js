import React, { Component } from 'react';
import {Navbar, Nav, NavItem} from 'react-bootstrap/lib/';
import MenuChildren from './menuChildren';
import './menu.css';

class Menu extends Component {
    componentDidMount() {
        // $('nav.topMenu .navbar-nav>li>a').on('hover', function(){
        //     let $link = $(this);
        //     $link.next().css('display', 'block');
        // })
    }
    render() {
        const items = this.props.items;
        return (
            <Navbar inverse collapseOnSelect className="topMenu">
                <Navbar.Header>
                    <Navbar.Toggle />
                </Navbar.Header>
                <Navbar.Collapse>
                    <Nav>
                        {items.map((item) => {
                            const isText = item.title === '|';
                            const hasSubChildren = item.children && item.children.length > 1 && item.children[0].children && item.children[0].children.length > 1
                            return ( isText ? 
                                <NavItem disabled key={item.title}>{item.title}</NavItem>
                                :
                                <MenuChildren item={item} key={item.title} hasSubChildren={hasSubChildren}/>
                            )
                        })}
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }
    
}
             
export default Menu;