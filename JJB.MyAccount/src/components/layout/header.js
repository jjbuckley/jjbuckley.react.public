import React, { Component } from 'react';
import {Grid, Row, Col, Dropdown, MenuItem} from 'react-bootstrap/lib/';
import {Form, Glyphicon, FormGroup, InputGroup, FormControl} from 'react-bootstrap/lib/';
import { Link } from 'react-router-dom'

import * as Api from '../utils/ApiProxy';
import Menu from './menu/menu';

import './header.css';

class Header extends Component {

    constructor(props) {
        super(props);
        this.state = {
            topMenu: []
        }
    }

    fetchData() {
        if (this.props.token && this.state.topMenu.length === 0) {
            Api.get('sitemap')
            .then(data => {
                this.setState({topMenu: data});
            })
            .catch(data => {
                console.log("Can't get top menu", data);
            })
        }
    }

    componentDidMount() {
        this.fetchData();
    }

    render() {
        if(this.props.token) this.fetchData();
        const topMenu = this.state.topMenu;
        const user = this.props.user;
        return (
            <header>
                <div className="account">
                    <Grid>
                        <Row className="show-grid text-right">
                            <Col md={12}>
                                <Dropdown id="dropdownButton" pullRight>
                                    <Dropdown.Toggle>
                                        My Account ({user ? user.FirstName : 'sign in'}) <Glyphicon glyph="user" />
                                    </Dropdown.Toggle>
                                    
                                    <Dropdown.Menu id="dropdownMenu">
                                        <li><Link to="/account">Account Overview</Link></li>
                                        <li><Link to="/account/my-wines">My Wines</Link></li>
                                        <MenuItem divider />
                                        <MenuItem onClick={this.props.invalidateUserData}>Sign out</MenuItem>
                                    </Dropdown.Menu>
                                        
                                </Dropdown>
                            </Col>          
                        </Row>
                    </Grid>
                </div>
                <div className="brand">
                    <Grid>
                        <Row>
                            <Col sm={4}>
                                <h1><a className="header-logo" href="/">JJBuckley</a></h1>  
                            </Col>
                            <Col sm={4}>
                                <p className="phone text-center">
                                    <span className="call-us"> Call us toll free</span><br/>
                                    <strong>888-859-4637</strong>
                                </p>
                            </Col>
                            <Col sm={4} className="search-bar text-right">
                                <Form inline>
                                    <a href="/cart"><strong>Cart (0) <Glyphicon glyph="shopping-cart" /></strong></a>&nbsp;                               
                                    <FormGroup>
                                        <InputGroup>
                                            <FormControl type="text" />
                                            <InputGroup.Addon>
                                                <Glyphicon glyph="search" />
                                            </InputGroup.Addon>
                                        </InputGroup>
                                    </FormGroup>
                                </Form>
                            </Col>
                        </Row>
                    </Grid>
                </div>
                <div className="promoLink">
                    <Grid>
                        <Row>
                            <Col md={12} className="text-center">
                                <a href="/signup" className="btn btn-primary btn-block">Sign up for curated daily wine deals</a>
                            </Col>
                        </Row>
                    </Grid>
                </div>
                <Menu items={topMenu}/>
            </header>
        );
    }
}
 
export default Header;
