import React, { Component } from 'react';
import * as Api from '../../utils/ApiProxy';

class WinesJJB extends Component {
  constructor(props){
    super(props);
    this.state = {
      wines: []
    }
  }

  componentDidMount() {
    const nsId = this.props.nsId;
    Api.get('customers/' + nsId + '/atJJBWines')
    .then(data => {
      let wines = data.map((item) => {
        return (
          <tr key={item.Sku}>
              <td>{item.Year}</td>
              <td>{item.WineName}</td>
              <td>{item.Varietal}</td>
              <td>{item.Size}</td>
              <td>{item.CurrentQty}</td>
          </tr>
        )
      });
      
      this.setState({wines: wines});
    })
  }

  render() {
    return (
          <table className="table table-striped table-hover table-condensed">
              <thead>
                  <tr>
                      <th>Vintage</th>
                      <th>Wine</th>
                      <th>Varietal</th>
                      <th>Size</th>
                      <th>Qty</th>
                  </tr>
              </thead>
              <tbody>
              {this.state.wines}
              </tbody>
          </table>

    );
  }
}

export default WinesJJB;
