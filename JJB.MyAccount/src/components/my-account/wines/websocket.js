import { Component } from 'react';

class SocketTest extends Component {
 
    constructor(props) {
        super(props);
        this.state = {date: new Date()};
      }

    componentDidMount() {

        var name = Date.now();
        var url = 'ws://jjbnotificationsocket.azurewebsites.net/NotificationMyWinesWSS.ashx?nsCustomerInternalId='+ name;
        let ws = new WebSocket(url);
        console.log(name);
        
        ws.onmessage =  function(data){console.log(data)};
      }

    render() {
        return "Web Socket";
      }
}

export default SocketTest;