export function handleResponse (response) {
    if (!response.ok || response.status >= 300) {
       return Promise.reject(response)
    }
    return response.json();
};

export function getToken() {
    return localStorage.getItem('api_access_token');
}

export function clearTokenData(){
    localStorage.removeItem('api_access_token');
    localStorage.removeItem('api_expires_in');
    localStorage.removeItem('api_token_type');
    localStorage.removeItem('api_user');
}

export function get(resource) {
    let authHash = getToken();
    let apiUrl = process.env.REACT_APP_JJBAPI_URL+ 'api/' + resource;

    return fetch(apiUrl, {
        headers: new Headers({
            "Authorization": "Bearer " + authHash,
            'Content-Type': 'application/json'
        })
    })
    .then(handleResponse)
    .catch(data => {
        throw data;
    })
}

export function post(resource, body) {
    let authHash = getToken();
    let apiUrl = process.env.REACT_APP_JJBAPI_URL + 'api/' + resource;

    return fetch(apiUrl, {
        method: 'POST',
        headers: new Headers({
            "Authorization": "Bearer " + authHash,
            'Content-Type': 'application/json'
        }),
        body: JSON.stringify(body)
    })
    .then(handleResponse)
    .catch(data => {
        throw data;
    })
}