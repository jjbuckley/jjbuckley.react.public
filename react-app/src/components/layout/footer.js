import React, { Component } from 'react';
import {Grid, Row, Col, Button} from 'react-bootstrap/lib/';

import * as Api from '../utils/ApiProxy';
import './footer.css';

class Footer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            bottomMenu: null
        }
    }

    componentDidMount() {
        Api.get('sitemap/footer')
        .then(data => {
            this.setState({bottomMenu: data});
        })
        .catch(data => {
            console.error("Can't get footer menu", data);
        })
    }

    render() {
        const menu = this.state.bottomMenu;
        return (
            <footer>
                <Grid>
                    <Row>
                        <a href="/trap/index.aspx" className="hidden">Honey Pot</a>                        
                        <a href="/customer-service/newsletter-signup" className="newsletter-link">
                            <Col md={9}>
                                <span className="signup">Sign up to receive our wine offers!</span>
                            </Col>
                            <Col md={3} className="text-center">
                                <Button bsStyle="primary" >Join our mailing list</Button>
                            </Col>
                        </a>
                    </Row>
                    {menu ?
                        <Row className="show-grid">
                            <Col md={3}>
                                <h4>JJ Buckley Fine Wines</h4>
                                <p className="copy">A pioneer in the early days of online wine retail, JJ Buckley Fine Wines remains an established leader in the world of web-based wine merchants. From our 43,000 square foot retail operation in Oakland, California, we offer premium and collectable wines, along with professional wine storage services, to discerning customers across the United States and around the world. We believe outstanding value, stellar selection, personal service, and professional knowledge are the cornerstones that have built our loyal following and reputation for excellence. JJ Buckley takes great pride in offering our customers honest, impartial, and expert wine advice. Our team of Wine Specialists is at your disposal, ready with personalized recommendations to suit your palate or occasion.</p>
                            </Col>
                            <Col md={2}>
                                <ul className="list-unstyled">
                                {menu.Wines.map((item) => {
                                    return(
                                        <li key={item.Text}>{item.IsTitle ? <a href={item.Url} className="title"><h4>{item.Text}</h4></a> : <a href={item.Url} className="link">{item.Text}</a> }</li>
                                    )
                                })}
                                </ul>
                            </Col>
                            <Col md={2}>
                                <ul className="list-unstyled">
                                {menu.AboutUs.map((item) => {
                                    return(
                                        <li key={item.Text}>{item.IsTitle ? <a href={item.Url} className="title"><h4>{item.Text}</h4></a> : <a href={item.Url} className="link">{item.Text}</a> }</li>
                                    )
                                })}
                                </ul>
                            </Col>
                            <Col md={2}>
                                <ul className="list-unstyled">
                                {menu.CustomerService.map((item) => {
                                    return(
                                        <li key={item.Text}>{item.IsTitle ? <a href={item.Url} className="title"><h4>{item.Text}</h4></a> : <a href={item.Url} className="link">{item.Text}</a> }</li>
                                    )
                                })}
                                </ul>
                            </Col>
                            <Col md={3} className="text-center contact">
                                <p className="call">Call us toll free</p>
                                <p className="big">
                                    <a href={"tel:"+menu.PhoneNumber}>{menu.PhoneNumber}</a>
                                </p>
                                <div className="address">
                                    <p>7305 Edgewater Drive</p>
                                    <p>Suite D</p>
                                    <p>Oakland, CA 94621</p>
                                </div>
                                <div className="social">
                                    <ul className="list-inline">
                                        <li><a href="http://www.facebook.com/pages/JJ-Buckley-Fine-Wines/106263506080646" className="facebook" target="_blank" rel="noopener noreferrer">Facebook</a></li>
                                        <li><a href="http://www.twitter.com/jjbuckleywines" className="twitter" target="_blank" rel="noopener noreferrer">Twitter</a></li>
                                        <li><a href="http://www.pinterest.com/jjbuckleywines/pins/" className="pinterest" target="_blank" rel="noopener noreferrer">Pinterest</a></li>
                                        <li><a href="http://www.youtube.com/user/jjbuckleywines" className="youtube" target="_blank" rel="noopener noreferrer">Youtube</a></li>
                                    </ul>
                                </div>
                                <a href="http://www.bbb.org/greater-san-francisco/business-reviews/wines-retail/j-j-buckley-fine-wines-in-oakland-ca-193233" target="_blank" rel="noopener noreferrer">
                                    <p className="footer-logo-bbb">
                                    </p>
                                </a>
                            </Col>
                            <Col md={12} className="text-center">
                                <div className="footer-cards"></div>
                                <p className="copy">© JJ Buckley Fine Wines, 7305 Edgewater Drive, Suite D, Oakland, California 94621 – ALL RIGHTS RESERVED<br/>
                                    <a href="/privacy-policy">Privacy policy</a> - 
                                    <a href="/legal">Legal</a> - 
                                    <a href="/customer-service/return-policy">Return policy</a>
                                </p>
                            </Col>
                        </Row>
                        : null
                    }
                    <Row className="show-grid">
                        
                    </Row>
                </Grid>
            </footer>
        );
    }
}
 
export default Footer;